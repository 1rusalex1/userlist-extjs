/**
 * The main application class. An instance of this class is created by app.js when it
 * calls Ext.application(). This is the ideal place to handle application launch and
 * initialization details.
 */
Ext.define('extapp.Application', {
    extend: 'Ext.app.Application',

    requires: [
        'Ext.util.Cookies'
    ],

    name: 'extapp',

    stores: [
        // TODO: add global / shared stores here
    ],



    views: [
        'extapp.view.login.Login',
        'extapp.view.main.Main'
    ],
    launch: function () {
        var loggedIn;
        loggedIn = Ext.util.Cookies.get("extappLoggedIn");
        Ext.create({
            xtype: (loggedIn != undefined && loggedIn == "true") ? 'app-main' : 'login'
        });
    },
    onAppUpdate: function () {
        Ext.Msg.confirm('Application Update', 'This application has an update, reload?',
            function (choice) {
                if (choice === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
