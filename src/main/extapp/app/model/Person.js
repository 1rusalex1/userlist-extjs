/**
 * Created by admin on 02.04.2016.
 */
Ext.define('extapp.model.Person', {
    extend: 'Ext.data.Model',
    fields: [{
        name: 'name',
        type: 'string'
    }, 'role']
});