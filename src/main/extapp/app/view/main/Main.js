/**
 * This class is the main view for the application. It is specified in app.js as the
 * "mainView" property. That setting automatically applies the "viewport"
 * plugin causing this view to become the body element (i.e., the viewport).
 *
 * TODO - Replace this content of this view to suite the needs of your application.
 */


Ext.define('extapp.view.main.Main', {
    extend: 'Ext.grid.Panel',
    xtype: 'app-main',
    requires: [
        'Ext.grid.plugin.RowEditing',
        'Ext.layout.container.Center',
        'Ext.tab.Panel',
        'Ext.toolbar.Separator',
        'extapp.store.Users'
    ],
    controller: 'main',
    store: {
        type: 'users'
    },
    width: 200,
    minHeight: 300,
    style: {
        margin: "0 auto"
    },
    frame: true,
    title: 'Users',
    iconCls: 'icon-user',
    flex: 1,
    columns: [{
        text: 'name',
        width: 75,
        sortable: true,
        dataIndex: 'name',
        editor: 'textfield'
    }, {
        header: 'Role',
        width: 120,
        sortable: true,
        dataIndex: 'role',
        editor: 'textfield'
    }],
    plugins: [{
        ptype: 'rowediting',
        pluginId: 'rowediting',
        clicksToEdit: 2,
        autoCancel: true,
        listeners: {
            cancelEdit: function (rowEditing, context) {
                // Canceling editing of a locally added, unsaved record: remove it
                if (context.record.phantom) {
                    Ext.data.StoreManager.lookup('usersId').remove(context.record);
                }
            }
        }
    }],
    renderTo: document.body,
    dockedItems: [{
        xtype: 'toolbar',
        items: [{
            text: 'Add',
            iconCls: 'icon-add',
            handler: function (component) {
                // empty record
                var grid = component.up('gridpanel');
                var rowEditing = grid.getPlugin('rowediting');
                Ext.data.StoreManager.lookup('usersId').insert(0, new extapp.model.Person());
                rowEditing.startEdit(0, 0);
            }
        }, '-', {
            itemId: 'delete',
            text: 'Delete',
            iconCls: 'icon-delete',
            handler: function (component) {
                var grid = component.up('gridpanel');
                var selection = grid.getView().getSelectionModel().getSelection()[0];
                if (selection) {
                    Ext.data.StoreManager.lookup('usersId').remove(selection);
                }
            }
        }]
    }],
    listeners: {
        afterrender: function (component) {
            var store = component.getStore();
            store.on('noLogin', function () {
                Ext.create({
                    xtype: 'login'
                });
                this.destroy();
            }, component);
        }
    }
});

