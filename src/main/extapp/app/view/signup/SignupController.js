/**
 * Created by admin on 01.04.2016.
 */
Ext.define('extapp.view.signup.SignupController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.signup',

    onSignupClick: function (button) {

        // This would be the ideal location to verify the user's credentials via
        // a server-side lookup. We'll just move forward for the sake of this example.

        var form = button.up('form');
        var win = button.up('window');
        if (form.isValid()) {
            form.submit({
                success: function (form, action) {
                    //var json = Ext.decode(action.result);
                    if (action.result["success"] == "false") {
                        console.log("in success false")
                        Ext.Msg.alert('Failed', action.result["message"]);
                    } else {
                        console.log("in success true")
                        Ext.create({
                            xtype: 'app-main'
                        });
                        win.destroy();
                    }

                },
                failure: function (form, action) {
                    Ext.Msg.alert('Failed', action.result.message);
                }
            });
        }
    }
});