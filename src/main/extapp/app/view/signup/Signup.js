/**
 * Created by admin on 01.04.2016.
 */
Ext.define('extapp.view.signup.Signup', {
    extend: 'Ext.window.Window',
    xtype: 'signup',
    draggable: false,
    requires: [
        'extapp.view.signup.SignupModel',
        'extapp.view.signup.SignupController',
        'Ext.form.Panel'
    ],
    controller: 'signup',
    bodyPadding: 10,
    title: 'Signup Window',
    closable: false,
    autoShow: true,
    viewModel: {
        type: 'signup'
    },
    items: {
        xtype: 'form',
        url: 'signup',
        method: 'POST',
        reference: 'form',
        items: [{
            xtype: 'textfield',
            name: 'name',
            fieldLabel: 'Name',
            allowBlank: false
        }, {
            xtype: 'textfield',
            name: 'password',
            inputType: 'password',
            fieldLabel: 'Password',
            allowBlank: false
        }, {
            xtype: 'displayfield',
            hideEmptyLabel: false,
            value: 'Enter any non-blank password'
        }],
        buttons: [{
            text: 'Signup',
            formBind: true,
            handler: "onSignupClick"
        }]
    }
});