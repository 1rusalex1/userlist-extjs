/**
 * Created by admin on 01.04.2016.
 */
Ext.define('extapp.view.login.Login', {
    extend: 'Ext.window.Window',
    xtype: 'login',
    draggable: false,
    requires: [
        'Ext.container.Container',
        'Ext.form.Panel',
        'Ext.form.field.Checkbox',
        'Ext.form.field.Display',
        'Ext.form.field.Text',
        'extapp.view.login.LoginController'
    ],
    controller: 'login',
    bodyPadding: 10,
    title: 'Login',
    closable: false,
    autoShow: true,
    flex: 1,
    items: {
        xtype: 'form',
        url: 'authenticate',
        reference: 'form',
        items: [{
            xtype: 'textfield',
            name: 'username',
            fieldLabel: 'Username',
            allowBlank: false
        }, {
            xtype: 'textfield',
            name: 'password',
            inputType: 'password',
            fieldLabel: 'Password',
            allowBlank: false
        }, {
            xtype: 'displayfield',
            hideEmptyLabel: false,
            value: 'Enter any non-blank password'
        }, {
            xtype: 'container',
            items: [
                {
                    xtype: 'displayfield',
                    hideEmptyLabel: true,
                    style: {
                        display: 'inline'
                    },
                    value: 'If you\'ve not signed up yet click the link:'
                },
                {
                    xtype: 'displayfield',
                    hideEmptyLabel: true,
                    value: 'Sign up',
                    style: {
                        display: 'inline',
                        marginLeft: '8px',
                        cursor: 'pointer'
                    },
                    fieldStyle: {
                        color: 'red'
                    },
                    listeners: {
                        afterrender: function (component) {
                            component.getEl()
                                .on('click', 'onSignupClick');
                        }
                    }
                }
            ]
        }],
        buttons: [{
            text: 'Login',
            formBind: true,
            handler: 'onLoginClick'

        }]
    },
    listeners: {
        beforeactivate: function (component) {
            Ext.util.Cookies.set("extappLoggedIn", false);
        }
    }
});