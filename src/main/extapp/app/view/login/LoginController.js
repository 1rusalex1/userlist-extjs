/**
 * Created by admin on 01.04.2016.
 */
Ext.define('extapp.view.login.LoginController', {
    extend: 'Ext.app.ViewController',
    alias: 'controller.login',
    onLoginClick: function (button) {
        var form = button.up('form');
        var win = button.up('window');
        if (form.isValid()) {
            form.submit({
                success: function (form, action) {
                    /*localStorage.setItem("extappLoggedIn", true);*/
                    Ext.create({
                        xtype: 'app-main'
                    });
                    win.destroy();
                },
                failure: function (form, action) {
                    Ext.Msg.alert("Message", "Check your login or password.");
                }
            });
        }
    },
    onSignupClick: function () {
        this.getView().destroy();
        Ext.create({
            xtype: 'signup'
        });
    }
});