/**
 * Created by admin on 02.04.2016.
 */
Ext.define('extapp.store.Users', {
    extend: 'Ext.data.Store',
    alias: 'store.users',
    requires: [
        'Ext.data.proxy.Rest',
        'extapp.model.Person'
    ],
    storeId: 'usersId',
    autoLoad: true,
    autoSync: true,
    model: 'extapp.model.Person',
    proxy: {
        type: 'rest',
        url: '/accounts/',
        headers: {
            'Content-Type': 'application/json'
        },
        reader: {
            type: 'json',
            rootProperty: 'data',
            metaProperty: 'error'
        },
        actionMethods: {
            create: 'POST',
            read: 'GET',
            update: 'PUT',
            destroy: 'DELETE'
        },
        api: {
            create: 'account/create',
            read: 'account/read'/*"data.json"*/,
            update: 'account/edit',
            destroy: 'account/delete'
        },
        listeners: {
            exception: function(proxy, response, operation) {
                var json = Ext.decode(response.responseText);
                if (json['type'] != undefined && json['type'] == 'message') {
                    Ext.Msg.alert('Message', json['message']);
                }
            }
        }
    },
    listeners: {
        load: function (store, records, successful, data) {
            var json = Ext.decode(data.getResponse().responseText);
            if (json['type'] != undefined && json['type'] == 'login') {
                store.fireEvent("noLogin");
            }
        }
    }
});