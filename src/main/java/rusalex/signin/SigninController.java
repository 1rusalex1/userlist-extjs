package rusalex.signin;




import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.Map;

@Controller
public class SigninController {


    @RequestMapping(value = "signin")
    @ResponseBody
    public Map<String,Object> signin(HttpServletResponse resp) {
        Cookie cookie = new Cookie("extappLoggedIn", "false");
        resp.addCookie(cookie);
        Map<String,Object> response = new HashMap<>();
        response.put("type","login");
        return response;
    }

    @RequestMapping(value = "signin/err")
    @ResponseBody
    public  Map<String,Object> signinerr(HttpServletResponse resp) {
        Cookie cookieLogIn = new Cookie("extappLoggedIn", "false");
        resp.addCookie(cookieLogIn);
        Map<String,Object> response = new HashMap<>();
        response.put("type","login");
        response.put("message","Incorrect login or password.");
        return response;
    }

    @RequestMapping(value = "signedin", method = RequestMethod.GET)
    @ResponseBody
    public Map<String,Object> signedin(HttpServletResponse response) {
        Cookie cookie = new Cookie("extappLoggedIn", "true");
        response.addCookie(cookie);
        Map<String,Object> map = new HashMap<>();
        map.put("success",true);
        map.put("loggedIn",true);
        map.put("username","admin");
        map.put("errorMessage",null);
        return map;
    }
}
