package rusalex.account;

import java.time.Instant;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javassist.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.annotation.Secured;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.*;


@Controller
class AccountController {

    private AccountRepository accountRepository;

    @Autowired
    public AccountController(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "account/read", method = RequestMethod.GET /*produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE*/)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Map<String, Object> getUsers() {
        Map<String, Object> response = new HashMap<>();
        response.put("data", accountRepository.findAll());
        return response;
    }

    @RequestMapping(value = "account/create", method = RequestMethod.POST /*produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE*/)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Map createUser() {
        Account account = new Account("defaultName" + Instant.now(), "1234", "ROLE_USER");
        account = accountService.save(account);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        Map<String, Object> response = new HashMap<>();
        response.put("success", "true");
        response.put("data", accounts);
        return response;
    }

    @RequestMapping(value = "account/edit/{id}", method = RequestMethod.PUT/*produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE*/)
    @ResponseStatus(value = HttpStatus.OK)
    @Transactional
    @ResponseBody
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Map editUser(@RequestBody Map<String,String> json, Errors errors) {
        Map<String, Object> response = new HashMap<>();
        Long id = Long.valueOf(json.get("id"));
        Account account = accountRepository.findOne(id);
        if (json.containsKey("role")) {
            account.setRole(json.get("role"));
        }
        if (json.containsKey("name")) {
            if (accountRepository.findOneByName(json.get("name"))!=null) {
                throw new Error("Username must be unique!");
            }
            account.setName(json.get("name"));
        }
        account = accountRepository.save(account);
        List<Account> accounts = new ArrayList<>();
        accounts.add(account);
        response.put("success", "true");
        response.put("data", accounts);
        return response;
    }

    @RequestMapping(value = "account/delete/{id}", method = RequestMethod.DELETE /*produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE*/)
    @ResponseStatus(value = HttpStatus.OK)
    @ResponseBody
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public Map<String, Object> deleteUser(@PathVariable ("id") Long id ) {
        accountRepository.delete(id);
        Map<String, Object> response = new HashMap<>();
        response.put("success", "true");
        return response;
    }
}
