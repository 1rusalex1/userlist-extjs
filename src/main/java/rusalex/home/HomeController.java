package rusalex.home;

import java.security.Principal;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
public class HomeController {
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index() {
		String str = "/WEB-INF/views/index.html";
		return str;
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = {"text/plain", "application/*"})
	public String index(@PathVariable ("id") String id) {
		String str = "/WEB-INF/views/"+id;
		return str;
	}
}
