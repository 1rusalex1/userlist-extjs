package rusalex.signup;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import rusalex.account.*;
import rusalex.support.web.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
public class SignupController {

    private static final String SIGNUP_VIEW_NAME = "signup/signup";

    @Autowired
    private AccountService accountService;

    @Autowired
    private AccountRepository accountRepository;


    @RequestMapping(value = "signup")
    public String signup(Model model) {
        model.addAttribute(new SignupForm());
        return SIGNUP_VIEW_NAME;
    }

    @RequestMapping(value = "signup", method = RequestMethod.POST, headers = "Content-Type=application/x-www-form-urlencoded")
    @Transactional
    @ResponseBody
    public Map<String, String> signup(@Valid @ModelAttribute SignupForm signupForm, Errors errors, RedirectAttributes ra) {
        Map<String, String> response = new HashMap<String, String>();
        if (errors.hasErrors()) {
            List<FieldError> fieldErrors = errors.getFieldErrors();
            String message = "";
            for (FieldError field : fieldErrors) {
                message = message + " " + field.getField();
            }
            response.put("message", message);
            return response;
        }
        if (accountRepository.findOneByName(signupForm.getName())!=null) {
            throw new Error("Username "+ signupForm.getName() + " already exists. Change another.");
        }
        Account account = accountService.save(signupForm.createAccount());
        accountService.signin(account);
        response.put("success","true");
        return response;
    }
}
